package starvation;

public class BusyWaiter implements Runnable {
    private final Table table;

    public BusyWaiter(Table table) {
        this.table = table;
    }

    @Override
    public void run() {
        while (true) {
            table.takeOrder(Thread.currentThread().getName()); // Immediately tries to take another order
        }
    }
}
