package starvation;

// The usual waiter waits after taking every order while the busy waiter immediately starts new order
// after finishing one. This leads to busy waiter using more resource than the usual waiter.
public class Main {

    public static void main(String[] args) {
        Table sharedTable = new Table();

        // Waiter threads - keep taking new orders, causing potential starvation for certain orders.
        for (int i = 1; i <= 3; i++) {
            new Thread(new Waiter(sharedTable), "Waiter " + i).start();
        }

        // The busy waiter tends to dominate the table, leading to starvation of others.
        new Thread(new BusyWaiter(sharedTable), "Busy Waiter").start();
    }
}