package starvation;

public class Table {
    // A synchronization lock for accessing the table
    private final Object tableLock = new Object();

    public void takeOrder(String waiterName) {
        synchronized (tableLock) {
            try {
                // Simulate the time taken to take an order
                System.out.println(waiterName + " is taking an order at the table.");
                Thread.sleep(100); // Taking an order takes 100ms
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}