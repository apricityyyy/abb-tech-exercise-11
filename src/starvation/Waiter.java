package starvation;

public class Waiter implements Runnable {
    private final Table table;

    public Waiter(Table table) {
        this.table = table;
    }

    @Override
    public void run() {
        while (true) {
            table.takeOrder(Thread.currentThread().getName());
            try {
                Thread.sleep(1000); // Wait for a bit before taking the next order
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}