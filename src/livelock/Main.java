package livelock;

// The idea is that a person selects option 1 from phone menu, and they are required to select option 2 to proceed
// When the user selects option 2, they are required to select option 1 to proceed.
public class Main {
    public static void main(String[] args) {
        PhoneMenu phoneMenu = new PhoneMenu();
        Caller caller = new Caller(phoneMenu);

        Thread callerThread = new Thread(caller);
        callerThread.start();
    }
}