package livelock;

public class PhoneMenu {
    private boolean optionOneSelected = false;
    private boolean optionTwoSelected = false;

    public synchronized void selectOptionOne() {
        optionOneSelected = true;
        System.out.println("You have selected Option 1. Please press 2 to continue.");
    }

    public synchronized void selectOptionTwo() {
        if (optionOneSelected) {
            optionTwoSelected = true;
            System.out.println("You have selected Option 2. Please press 1 to continue.");
        }
    }

    public synchronized boolean isLooping() {
        boolean looping = optionOneSelected && optionTwoSelected;
        if (looping) {
            // Reset the selections to simulate the caller going through the options again
            optionOneSelected = false;
            optionTwoSelected = false;
        }
        return looping;
    }
}
