package livelock;

public class Caller implements Runnable {
    private final PhoneMenu phoneMenu;

    public Caller(PhoneMenu phoneMenu) {
        this.phoneMenu = phoneMenu;
    }

    @Override
    public void run() { // simulating the process 5 times
        int count = 0;

        while (count < 5) {
            phoneMenu.selectOptionOne();
            phoneMenu.selectOptionTwo();
            if (phoneMenu.isLooping()) {
                System.out.println("Caller is caught in a livelock, navigating through the menu options endlessly.");
            }
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
                break;
            }

            count++;
        }
    }
}